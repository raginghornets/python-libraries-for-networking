# Import the yamltodict library
import yaml

# Open the sample yaml file and read it into variable
with open("yaml_example.yaml") as f:
  yaml_example = f.read()

# Print the raw yaml data
print("Raw YAML data:\n", yaml_example)

# Parse the yaml into a Python dictionary
yaml_dict = yaml.load(yaml_example)

# Save the interface name into a variable
interface_name = yaml_dict["interface"]["name"]

# Print the interface name
print("\nInterface name:\n", interface_name)

# Change the IP address of the interface
yaml_dict["interface"]["ipv4"]["address"]["ip"] = "192.168.0.2"

# Revert to the YAML string version of the dictionary
print("\nModified YAML data:\n", yaml.dump(yaml_dict, default_flow_style=False))